﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void Transaction()
        {
            Transaction t = new Transaction(5);
            Assert.AreEqual(5, t.amount);
            Assert.AreEqual(DateTime.Now.Date, t.transactionDate.Date);
        }
    }
}
