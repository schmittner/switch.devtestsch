﻿using NUnit.Framework;
using System;
using System.Linq;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        /// <summary>
        /// Test customer statemen generation
        /// </summary>
        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n\nChecking Account\n  deposit $100,00\nTotal $100,00\n\nSavings Account\n  deposit $4.000,00\n  withdrawal $200,00\nTotal $3.800,00\n\nTotal In All Accounts $3.900,00", henry.GetStatement());
        }

        /// <summary>
        /// Test customer statemen generation with accrue
        /// </summary>
        [Test]
        public void TestStatementForAccountWithAccrue()
        {
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);
            Account maxSavingsAccount = new Account(Account.MAXI_SAVINGS);

            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount)
                .OpenAccount(savingsAccount)
                .OpenAccount(maxSavingsAccount);

            checkingAccount.Deposit(1000.0, DateTime.Now.AddDays(-10));
            savingsAccount.Deposit(4000.0, DateTime.Now.AddDays(-9));
            savingsAccount.Withdraw(200.0, DateTime.Now.AddDays(-8));
            checkingAccount.Deposit(100.0, DateTime.Now.AddDays(-7));
            checkingAccount.Withdraw(100.0, DateTime.Now.AddDays(-7));
            maxSavingsAccount.Deposit(5000, DateTime.Now.AddDays(-5));

            string statement = henry.GetDailyAccrueStatement();
            var lines = statement.Split('\n');

            Assert.AreEqual(lines.Where(x => x.Equals("Checking Account", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Date: 09/06/2019  Deposit $1.000,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Date: 12/06/2019  Deposit $100,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Date: 12/06/2019  Withdrawal $100,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Subtotal $1.000,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Interests $0,03", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Total $1.000,03", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);

            Assert.AreEqual(lines.Where(x => x.Equals("Savings Account", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Date: 10/06/2019  Deposit $4.000,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Date: 11/06/2019  Withdrawal $200,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Subtotal $3.800,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);

            Assert.AreEqual(lines.Where(x => x.Equals("Maxi Savings Account", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Date: 14/06/2019  Deposit $5.000,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
            Assert.AreEqual(lines.Where(x => x.Equals("Subtotal $5.000,00", StringComparison.InvariantCultureIgnoreCase)).Count(), 1);
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(Account.SAVINGS));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Account.SAVINGS));
            oscar.OpenAccount(new Account(Account.CHECKING));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Account.SAVINGS));
            oscar.OpenAccount(new Account(Account.CHECKING));
            oscar.OpenAccount(new Account(Account.MAXI_SAVINGS));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        /// <summary>
        /// Test transfer between accounts
        /// </summary>
        [Test]
        public void TestTransfer()
        {
            // create accounts 
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);

            // open accounts
            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            // deposit
            checkingAccount.Deposit(1000.0);
            savingsAccount.Deposit(1000.0);

            // transfer amount between accounts
            henry.Transfer(checkingAccount, savingsAccount, 500);

            // check from
            Assert.AreEqual(checkingAccount.SumTransactions(), 500);
            // check to
            Assert.AreEqual(savingsAccount.SumTransactions(), 1500);

            // Test not available amount
            checkingAccount.Withdraw(500);

            var ex = Assert.Throws<ArgumentException>(() => henry.Transfer(checkingAccount, savingsAccount, 500));
            Assert.That(ex.Message, Is.EqualTo("not enough money available in from account"));
        }
    }
}