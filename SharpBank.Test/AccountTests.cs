using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTests
    {
        /// <summary>
        /// Test interested erarned
        /// </summary>
        [Test]
        public void TestInterestEarned()
        {
            // create accounts 
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);

            // open accounts
            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount)
                .OpenAccount(savingsAccount)
                .OpenAccount(maxiSavingsAccount);

            // deposit
            checkingAccount.Deposit(10000.0);
            savingsAccount.Deposit(1000.0);
            maxiSavingsAccount.Deposit(1000.0);

            double total = checkingAccount.InterestEarned();
            Assert.AreEqual(total, checkingAccount.SumTransactions() * 0.001);

            total = maxiSavingsAccount.InterestEarned();
            Assert.AreEqual(total, maxiSavingsAccount.SumTransactions() * 0.05);

            maxiSavingsAccount.Withdraw(100.0);
            total = maxiSavingsAccount.InterestEarned();
            Assert.AreEqual(total, maxiSavingsAccount.SumTransactions() * 0.001);
        }

        /// <summary>
        /// Test HasWithdrawals
        /// </summary>
        [Test]
        public void TestHasWithdrawals()
        {
            // create accounts 
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);

            // open accounts
            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount)
                .OpenAccount(savingsAccount)
                .OpenAccount(maxiSavingsAccount);

            // only deposit
            checkingAccount.Deposit(10000.0);
            savingsAccount.Deposit(1000.0);
            maxiSavingsAccount.Deposit(1000.0);

            Assert.AreEqual(false, checkingAccount.HasWithdrawals());
            Assert.AreEqual(false, savingsAccount.HasWithdrawals());
            Assert.AreEqual(false, maxiSavingsAccount.HasWithdrawals());

            // Withdraw
            checkingAccount.Withdraw(100.0);
            savingsAccount.Withdraw(100.0);
            maxiSavingsAccount.Withdraw(100.0);

            Assert.AreEqual(true, checkingAccount.HasWithdrawals());
            Assert.AreEqual(true, savingsAccount.HasWithdrawals());
            Assert.AreEqual(true, maxiSavingsAccount.HasWithdrawals());
        }

        /// <summary>
        /// Test HasWithdrawals with date
        /// </summary>
        [Test]
        public void TestHasWithdrawalsDate()
        {
            // create accounts 
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);

            // open accounts
            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount)
                .OpenAccount(savingsAccount)
                .OpenAccount(maxiSavingsAccount);

            // only deposit
            checkingAccount.Deposit(10000.0, DateTime.Now.AddDays(-10));
            savingsAccount.Deposit(1000.0, DateTime.Now.AddDays(-5));
            maxiSavingsAccount.Deposit(1000.0, DateTime.Now.AddDays(-3));

            Assert.AreEqual(false, checkingAccount.HasWithdrawals());
            Assert.AreEqual(false, savingsAccount.HasWithdrawals());
            Assert.AreEqual(false, maxiSavingsAccount.HasWithdrawals());

            Assert.AreEqual(false, checkingAccount.HasWithdrawals(DateTime.Now.AddDays(-10)));
            Assert.AreEqual(false, savingsAccount.HasWithdrawals(DateTime.Now.AddDays(-5)));
            Assert.AreEqual(false, maxiSavingsAccount.HasWithdrawals(DateTime.Now.AddDays(-3)));

            // Withdraw more than 10 days
            checkingAccount.Withdraw(100.0, DateTime.Now.AddDays(-12));
            savingsAccount.Withdraw(100.0, DateTime.Now.AddDays(-12));
            maxiSavingsAccount.Withdraw(100.0, DateTime.Now.AddDays(-12));

            Assert.AreEqual(false, checkingAccount.HasWithdrawals(DateTime.Now));
            Assert.AreEqual(false, savingsAccount.HasWithdrawals(DateTime.Now));
            Assert.AreEqual(false, maxiSavingsAccount.HasWithdrawals(DateTime.Now));

            // Withdraw less than 10 days
            checkingAccount.Withdraw(100.0, DateTime.Now.AddDays(-9));
            savingsAccount.Withdraw(100.0, DateTime.Now.AddDays(-9));
            maxiSavingsAccount.Withdraw(100.0, DateTime.Now.AddDays(-9));

            // today
            Assert.AreEqual(true, checkingAccount.HasWithdrawals(DateTime.Now));
            Assert.AreEqual(true, savingsAccount.HasWithdrawals(DateTime.Now));
            Assert.AreEqual(true, maxiSavingsAccount.HasWithdrawals(DateTime.Now));

            // days before
            Assert.AreEqual(true, checkingAccount.HasWithdrawals(DateTime.Now.AddDays(-3)));
            Assert.AreEqual(true, savingsAccount.HasWithdrawals(DateTime.Now.AddDays(-3)));
            Assert.AreEqual(true, maxiSavingsAccount.HasWithdrawals(DateTime.Now.AddDays(-3)));
        }

        /// <summary>
        /// Test deposits with date
        /// </summary>
        [Test]
        public void TestDepositDate()
        {
            // create accounts 
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);

            // open accounts
            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount)
                .OpenAccount(savingsAccount)
                .OpenAccount(maxiSavingsAccount);

            // deposit
            checkingAccount.Deposit(10000.0, DateTime.Now.AddDays(-1));
            Assert.AreEqual(checkingAccount.SumTransactions(), 10000.0);

            savingsAccount.Deposit(1000.0, DateTime.Now.AddDays(-1));
            Assert.AreEqual(savingsAccount.SumTransactions(), 1000.0);

            maxiSavingsAccount.Deposit(2000.0, DateTime.Now.AddDays(-1));
            Assert.AreEqual(maxiSavingsAccount.SumTransactions(), 2000.0);

            var ex1 = Assert.Throws<ArgumentException>(() => checkingAccount.Deposit(10001.0, DateTime.Now.AddDays(1)));
            Assert.That(ex1.Message, Is.EqualTo("date must be lower or equal than today"));
            Assert.AreEqual(checkingAccount.SumTransactions(), 10000.0);

            var ex2 = Assert.Throws<ArgumentException>(() => savingsAccount.Deposit(1001.0, DateTime.Now.AddDays(1)));
            Assert.That(ex2.Message, Is.EqualTo("date must be lower or equal than today"));
            Assert.AreEqual(savingsAccount.SumTransactions(), 1000.0);

            var ex3 = Assert.Throws<ArgumentException>(() => maxiSavingsAccount.Deposit(2001.0, DateTime.Now.AddDays(1)));
            Assert.That(ex3.Message, Is.EqualTo("date must be lower or equal than today"));
            Assert.AreEqual(maxiSavingsAccount.SumTransactions(), 2000.0);
        }

        /// <summary>
        /// Test Withdraw with date
        /// </summary>
        [Test]
        public void TestWithdrawDate()
        {
            // create accounts 
            Account checkingAccount = new Account(Account.CHECKING);
            Account savingsAccount = new Account(Account.SAVINGS);
            Account maxiSavingsAccount = new Account(Account.MAXI_SAVINGS);

            // open accounts
            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount)
                .OpenAccount(savingsAccount)
                .OpenAccount(maxiSavingsAccount);

            // deposit
            checkingAccount.Deposit(10000.0, DateTime.Now.AddDays(-1));
            checkingAccount.Withdraw(5000.0, DateTime.Now.AddDays(-1));
            Assert.AreEqual(checkingAccount.SumTransactions(), 5000.0);

            savingsAccount.Deposit(1000.0, DateTime.Now.AddDays(-1));
            savingsAccount.Withdraw(500.0, DateTime.Now.AddDays(-1));
            Assert.AreEqual(savingsAccount.SumTransactions(), 500.0);

            maxiSavingsAccount.Deposit(2000.0, DateTime.Now.AddDays(-1));
            maxiSavingsAccount.Withdraw(500.0, DateTime.Now.AddDays(-1));
            Assert.AreEqual(maxiSavingsAccount.SumTransactions(), 1500.0);

            var ex1 = Assert.Throws<ArgumentException>(() => checkingAccount.Withdraw(10001.0, DateTime.Now.AddDays(1)));
            Assert.That(ex1.Message, Is.EqualTo("date must be lower or equal than today"));
            Assert.AreEqual(checkingAccount.SumTransactions(), 5000.0);

            var ex2 = Assert.Throws<ArgumentException>(() => savingsAccount.Withdraw(1001.0, DateTime.Now.AddDays(1)));
            Assert.That(ex2.Message, Is.EqualTo("date must be lower or equal than today"));
            Assert.AreEqual(savingsAccount.SumTransactions(), 500.0);

            var ex3 = Assert.Throws<ArgumentException>(() => maxiSavingsAccount.Withdraw(2001.0, DateTime.Now.AddDays(1)));
            Assert.That(ex3.Message, Is.EqualTo("date must be lower or equal than today"));
            Assert.AreEqual(maxiSavingsAccount.SumTransactions(), 1500.0);
        }
    }
}
