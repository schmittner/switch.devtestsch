﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public class Account
    {
        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;
        private const int WITHDRAWS_DAYS = 10;

        private readonly int accountType;
        public List<Transaction> transactions;

        public Account(int accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        /// <summary>
        /// Deposit in an account for a date
        /// </summary>
        /// <param name="amount">Amount to deposit</param>
        /// <param name="date">Transaction date</param>
        public void Deposit(double amount, DateTime date)
        {
            // check amount
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                // check date
                if (date.Date > DateTime.Now)
                {
                    throw new ArgumentException("date must be lower or equal than today");
                }
                else
                {
                    // add transaction
                    transactions.Add(new Transaction(amount, date));
                }
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }

        /// <summary>
        /// Deposit from an account for a date
        /// </summary>
        /// <param name="amount">Amount to withdraw</param>
        /// <param name="date">Transaction date</param>
        public void Withdraw(double amount, DateTime date)
        {
            // check amount
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                // check date
                if (date.Date > DateTime.Now)
                {
                    throw new ArgumentException("date must be lower or equal than today");
                }
                else
                {
                    // add transaction
                    transactions.Add(new Transaction(-amount, date));
                }
            }
        }

        public double InterestEarned()
        {
            double amount = SumTransactions();
            switch (accountType)
            {
                case SAVINGS:
                    if (amount <= 1000)
                        return amount * 0.001;
                    else
                        return 1 + (amount - 1000) * 0.002;
                case MAXI_SAVINGS:
                    // check Withdrawal in past 10 days
                    if (HasWithdrawals())
                        return amount * 0.001;
                    else
                        return amount * 0.05;
                default:
                    return amount * 0.001;
            }
        }

        /// <summary>
        /// Calculate daily interest from a total amount. Based on 365 days.
        /// </summary>
        /// <param name="amount">Amount to calculate daily interest</param>
        /// <returns></returns>
        public double DayInterestEarned(double amount, DateTime date)
        {
            switch (accountType)
            {
                case SAVINGS:
                    if (amount <= 1000)
                        return (amount * 0.001)/365;
                    else
                        return (1 + (amount - 1000) * 0.002) / 365;
                case MAXI_SAVINGS:
                    // check Withdrawal in past 10 days
                    if (HasWithdrawals(date))
                        return amount * 0.001 / 365;
                    else
                        return amount * 0.05 / 365;
                default:
                    return amount * 0.001 / 365;
            }
        }

        public double SumTransactions()
        {
            return CheckIfTransactionsExist(true);
        }

        private double CheckIfTransactionsExist(bool checkAll)
        {
            double amount = 0.0;
            foreach (Transaction t in transactions)
                amount += t.amount;
            return amount;
        }

        /// <summary>
        /// Sum transactions with daily accure
        /// </summary>
        /// <returns>Total</returns>
        public double SumTransactionsWithAccure()
        {
            double total = 0.0;

            // get first account transaction
            Transaction first = transactions.OrderBy(x => x.transactionDate).FirstOrDefault();

            // if exists
            if (first != null)
            {
                // loop through each date
                for (DateTime d = first.transactionDate; d.Date <= DateTime.Now.Date; d = d.AddDays(1))
                {
                    // search date transactions
                    foreach (Transaction t in transactions.Where(x => x.transactionDate.Date == d.Date))
                    {
                        // sum amount to total
                        total += t.amount;
                    }

                    // calculate day interest earned and sum to total
                    total += DayInterestEarned(total, d);
                }
            }

            return total;
        }

        /// <summary>
        /// Return Account type
        /// </summary>
        /// <returns>Enumerable of account type</returns>
        public int GetAccountType()
        {
            return accountType;
        }

        /// <summary>
        /// Check if an account has Withdrawal in past X days from today
        /// </summary>
        /// <returns>True/False</returns>
        public bool HasWithdrawals()
        {
            // at least one transaction with negative amount
            return transactions.Where(x => x.transactionDate.Date >= DateTime.Now.AddDays(-WITHDRAWS_DAYS).Date && x.amount < 0).Count() > 0;
        }

        /// <summary>
        /// Check if an account has Withdrawals in last X days from date
        /// </summary>
        /// <param name="date">Date to search from</param>
        /// <returns></returns>
        public bool HasWithdrawals(DateTime date)
        {
            // at least one transaction with negative amount
            return transactions.Where(x => x.transactionDate.Date >= date.AddDays(-WITHDRAWS_DAYS).Date && x.amount < 0).Count() > 0;
        }
    }
}
