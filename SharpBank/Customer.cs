﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpBank
{
    public class Customer
    {
        private string name;
        private List<Account> accounts;

        public Customer(string name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public string GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public string GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            string statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        /// <summary>
        /// Get statement with daily interest calculation
        /// </summary>
        /// <returns>String output</returns>
        public string GetDailyAccrueStatement()
        {
            string statement = null;
            double total = 0.0;

            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccountWithAccrue(a) + "\n";
                total += a.SumTransactionsWithAccure();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private string StatementForAccount(Account a)
        {
            string s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case Account.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        /// <summary>
        /// Create statement for an account with daily accrue
        /// </summary>
        /// <param name="a">Account to generate the statement</param>
        /// <returns>String output</returns>
        private string StatementForAccountWithAccrue(Account a)
        {
            string s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case Account.CHECKING:
                    s += "Checking Account\n";
                    break;
                case Account.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case Account.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            double interest = 0.0;
            double dailyInterest = 0.0;
            
            // Get first movement
            Transaction first = a.transactions.OrderBy(x => x.transactionDate).FirstOrDefault();

            // if exists
            if (first != null)
            {
                // loop through dates
                for (DateTime d = first.transactionDate; d.Date <= DateTime.Now.Date; d = d.AddDays(1))
                {
                    // search date transactions
                    foreach (Transaction t in a.transactions.Where(x=>x.transactionDate.Date == d.Date))
                    {
                        s += "Date: " + d.ToShortDateString() + "  " + (t.amount < 0 ? "Withdrawal" : "Deposit") + " " + ToDollars(t.amount) + "\n";
                        total += t.amount;
                    }

                    // calculate day interest earned
                    dailyInterest = a.DayInterestEarned(total, d);
                    // sum interest + daily interest
                    interest += dailyInterest;
                    // sum total + daily interest
                    total += dailyInterest;
                }
            }

            // print subtotal
            s += "Subtotal " + ToDollars(total-interest) + "\n";
            // print interest
            s += "Interests " + ToDollars(interest) + "\n";
            // print total
            s += "Total " + ToDollars(total);
            return s;
        }

        private string ToDollars(double d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }

        /// <summary>
        /// Transfer between acounts
        /// </summary>
        /// <param name="from">Account from where to transfer</param>
        /// <param name="to">Destination account</param>
        /// <param name="amount">Amount to transfer</param>
        public void Transfer(Account from, Account to, double amount)
        {
            // check amount
            if (amount <= 0)
            {
                // generate managed exception
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                // check available amount in "from" account 
                if (from.SumTransactions() >= amount)
                {
                    // withdraw the amount from account
                    from.Withdraw(amount);

                    // deposit the amount in account
                    to.Deposit(amount);
                }
                else
                {
                    // generate managed exception
                    throw new ArgumentException("not enough money available in from account");
                }
            }
        }
    }
}
