﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double amount;

        public readonly DateTime transactionDate;

        public Transaction(double amount)
        {
            this.amount = amount;
            this.transactionDate = DateTime.Now;
        }

        public Transaction(double amount, DateTime date)
        {
            this.amount = amount;
            this.transactionDate = date;
        }

    }
}
